package com.farmcart.mailservice.maildatastore;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.farmcart.mailservice.maildao.MailDAO;
import com.farmcart.mailservice.model.MailDetailsBean;

public class MailFileReader {

	public void readFile(String path) {
		
		File file=new File(path);
		MailDetailsBean mailBean;
		
		ApplicationContext ac= new ClassPathXmlApplicationContext("applicationContext.xml");
		
		MailDAO object=(MailDAO)ac.getBean("maildao");
		
		try(BufferedReader br=new BufferedReader(new FileReader(file)))
		{
			String line;
			while((line=br.readLine())!=null)
			{
				String []split=line.split(",");
				mailBean = new MailDetailsBean();
				mailBean.setName(split[0]);
				mailBean.setEmailID(split[1]);
				mailBean.setMessage(split[2]);
				object.addEntry(mailBean);
			}
		}
		catch(Exception io)
		{
			io.printStackTrace();
		}
	}

}
