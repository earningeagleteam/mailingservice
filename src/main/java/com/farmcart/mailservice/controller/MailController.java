package com.farmcart.mailservice.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.farmcart.mailservice.maildao.MailDAO;
import com.farmcart.mailservice.maildatastore.MailActivity;
import com.farmcart.mailservice.model.MailDetailsBean;


@RestController
public class MailController {
	
	@Autowired
	EmailController emailController;
	
	MailActivity maildatastore;
	
	ApplicationContext ac= new ClassPathXmlApplicationContext("applicationContext.xml");
	
	MailDAO object=(MailDAO)ac.getBean("maildao");
	
	@RequestMapping("/mail/{activity}")
	public void executeActivity(@PathVariable Integer activity)
	{
		maildatastore=new MailActivity();
		maildatastore.startActivity(activity);
	}
	
	@RequestMapping("/display")
	public List<MailDetailsBean> display()
	{
		
		List<MailDetailsBean> list=object.getEntries();
		
		return list;
	}
	
	@RequestMapping("/sendemail")
	public String sendEmail()
	{
		return emailController.processRequet();
	}

}
