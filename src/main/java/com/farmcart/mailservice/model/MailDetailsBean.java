package com.farmcart.mailservice.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class MailDetailsBean {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;
	private String name;
	private String message;
	private String emailID;
	public MailDetailsBean() {
		
	}
	public MailDetailsBean(Integer id, String name, String message, String emailID) {
		super();
		this.id = id;
		this.name = name;
		this.message = message;
		this.emailID = emailID;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getEmailID() {
		return emailID;
	}
	public void setEmailID(String emailID) {
		this.emailID = emailID;
	}
	@Override
	public String toString() {
		return "MailDetailsBean [id=" + id + ", name=" + name + ", message=" + message + ", emailID=" + emailID + "]";
	}
}
