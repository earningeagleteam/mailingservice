package com.farmcart.mailservice.maildao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import javax.activation.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.farmcart.mailservice.model.MailDetailsBean;

@Repository
public class MailDAO {
	private JdbcTemplate jdbcTemplate;

	public void setJdbcTemplate(JdbcTemplate jdbcTemplate)
	{
		this.jdbcTemplate=jdbcTemplate;
	}
	
	public int addEntry(MailDetailsBean maildetails) {

		String query="insert into mail_details_bean(name, message, emailID) values (?,?,?)";
		return jdbcTemplate.update(query,maildetails.getName(), maildetails.getMessage(), maildetails.getEmailID());
	}
	
	public int updateEntry(int id, MailDetailsBean maildetails) {

		String query="update mail_details_bean set name=?, message=?, emailID=? where id=?";
		return jdbcTemplate.update(query,maildetails.getName(), maildetails.getMessage(), maildetails.getEmailID(),id);
	}
	
	public int deleteEntry(int id)
	{
		String query="delete from mail_details_bean where id=?";
		return jdbcTemplate.update(query,id);
	}
	
	public List<MailDetailsBean> getEntries()
	{
		String query="select * from mail_details_bean";
		
		RowMapper<MailDetailsBean> rowMapper=new MailDetailsRowMapper();
		
		return jdbcTemplate.query(query, rowMapper);
	}

}

class MailDetailsRowMapper implements RowMapper<MailDetailsBean>
{

	@Override
	public MailDetailsBean mapRow(ResultSet rs, int rowNum) throws SQLException {
		MailDetailsBean details=new MailDetailsBean();
		
		details.setId(rs.getInt("id"));
		details.setName(rs.getString("name"));
		details.setMessage(rs.getString("message"));
		details.setEmailID(rs.getString("emailID"));
		
		return details;
	}
	
}